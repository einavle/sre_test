FROM python:2.7.14

RUN pip install pystrich

ADD setup.py /

ADD myapi /myapi

EXPOSE 8080

CMD [ "python", "-m", "SimpleHTTPServer", "8080" ]
